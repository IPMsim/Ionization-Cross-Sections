#!/usr/bin/env bash

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname ${SCRIPT}`
VERSIONFILE="$SCRIPTPATH/ionics/VERSION"

echo -n "$1" > ${VERSIONFILE}
git add ${VERSIONFILE}
git commit -m "New version $1"
