# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from setuptools import setup


def readme():
    with open('README.rst') as fp:
        return fp.read()


def version():
    with open('ionics/VERSION') as f:
        return f.read()


setup(
    name='ionics',
    version=version(),
    description='This package contains various ionization cross section models.',
    long_description=readme(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Topic :: Scientific/Engineering :: Physics',
    ],
    keywords='ionization',
    url='https://gitlab.com/IPMsim/Ionization-Cross-Sections',
    author='Dominik Vilsmeier',
    author_email='d.vilsmeier@gsi.de',
    license='GPL-3.0',
    packages=[
        'ionics',
        'ionics/ddcs',
        'ionics/gui',
        'ionics/material_data',
        'ionics/sdcs',
    ],
    install_requires=[
        'numpy',
        'scipy',
    ],
    include_package_data=True,
    zip_safe=False
)
