from __future__ import print_function, unicode_literals

import glob
import json
import re
import sys

if not (sys.version_info.major == 3 and sys.version_info.minor >= 5):
    raise EnvironmentError('This script needs to be run with at least Python 3.5')

ENCODING_HEADER = "# -*- coding: utf-8 -*-"
LICENSE_NOTE = \
"""
#    `ionics` is a software which models various ionization cross sections.
#    Copyright (C) 2017  Dominik Vilsmeier
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

ENCODING_PLUS_LICENSE = '{0}\n{1}'.format(ENCODING_HEADER, LICENSE_NOTE)


def apply_license_note(filepath):
    encoding_header_missing = False
    with open(filepath) as fp:
        header = fp.readline()
        if header.strip() != ENCODING_HEADER:
            encoding_header_missing = True
    with open(filepath) as fp:
        content = fp.read()
    if LICENSE_NOTE in content:
        print('[INFO] %s already contains the license note' % filepath)
        return
    if encoding_header_missing:
        if not content:
            content = ENCODING_PLUS_LICENSE + '\n'
        else:
            print('[WARNING] Encoding header is missing for %s' % filepath)
            content = ENCODING_PLUS_LICENSE + '\n\n' + content
    else:
        content = re.sub(r'# -\*- coding: utf-8 -\*-', ENCODING_PLUS_LICENSE, content)
    with open(filepath, 'w') as fp:
        fp.write(content)


if __name__ == '__main__':
    source_file_paths = glob.glob('ionics/**/*.py', recursive=True)
    print('Adding license note to the following files: ')
    print(json.dumps(source_file_paths, indent=4))

    for source_file in source_file_paths:
        apply_license_note(source_file)
